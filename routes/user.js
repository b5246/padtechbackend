const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');
const auth = require('../auth');

// Register system User
router.post("/register", (req,res)=>{
	//if(auth.decode(req.headers.authorization).isAdmin){
		userController.registerUser(req.body).then(resultFromController=> res.send(resultFromController));
	//}else res.send('Restricted Access.')
})

// Route to Create Teacher's Record 
router.post("/enroll", auth.verify, (req,res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		userController.registerRecord(req.body).then(resultFromController=> res.send(resultFromController));
	}else res.send('Restricted Access.')
})

// Route to Access All Teacher's Record 
router.get("/all",auth.verify, (req,res) => {
	if(auth.decode(req.headers.authorization).isAdmin){
		userController.getAllRecords().then(resultFromController => res.send(resultFromController));
	}else res.send('Restricted Access.')
})


// Route to Update Teacher's Record 
router.put("/update/:recordId", auth.verify,(req,res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		userController.updateRecord(req.params,req.body).then(resultFromController=> res.send(resultFromController))
	}else res.send('Restricted Access.')
})

// Route to Delete a Teacher's Record 
router.delete("/delete/:recordId",auth.verify,(req,res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		userController.deleteRecord(req.params).then(resultFromController=>res.send(resultFromController))
	}else{res.send('Restricted Access.')}	
})

//Route for Authenticated Login
router.post("/login", (req,res)=>{
	userController.loginUser(req.body).then(resultFromController=> res.send(resultFromController))
})

//Route access by Record ID
router.get("/record/:recordId", auth.verify,(req,res)=>{
	userController.getRecordById(req.params).then(resultFromController=> res.send(resultFromController))
})

// Route access User by Id
router.get("/details", auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	userController.getUserById(userData.id).then(resultFromController=> res.send(resultFromController))
})



module.exports = router