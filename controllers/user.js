const User = require("../models/User")
const Record = require("../models/Record")
const bcrypt = require("bcrypt")
const auth = require("../auth")

// Register User
module.exports.registerUser = data=>{
	let newUser = new User({
		fullName: data.fullName,
		password: bcrypt.hashSync(data.password,10),
		isAdmin: data.isAdmin,
	})
	return newUser.save().then((result,error)=>{
		if(error){
			console.log(error)
			return false
		}
		else{return true}
	})
}

// Create a Record
module.exports.registerRecord = data=>{
	let newRecord = new Record({
		fullName: data.fullName,
		major:data.major,
		dateHired: data.dateHired
	})
	return newRecord.save().then((result,error)=>{
		if(error){
			console.log(error)
			return false
		}
		else{return true}
	})
}

// Read All Records
module.exports.getAllRecords=()=>{
	return Record.find({}).then(result => {
		return result
	})
}

// Update User
module.exports.updateRecord = (reqParams,data)=>{
	let recordUpdate = {
		fullName: data.fullName,
		major: data.major,
		dateHired: data.dateHired
	}
	return Record.findByIdAndUpdate(reqParams.recordId, recordUpdate).then((result,error)=>{
		if(error){
			return false
		}else {return true}
	})
}

// Delete Record
module.exports.deleteRecord = (reqParams,data)=>{
	return Record.findById(reqParams.recordId).then((result,error)=>{
		if(error){
			return false
		}else {
			result.deleteOne()
			return true}
	})
}

//Login
module.exports.loginUser = data=>{
	return User.findOne({fullName:data.fullName}).then(result=>{
		if(result==null){return false}
		else{
			const isPasswordCorrect = bcrypt.compareSync(data.password,result.password)
			if(isPasswordCorrect){return{access:auth.createAccessToken(result)}} else {return false}
		}
	})
}

// Access user by ID
module.exports.getUserById = reqbody=>{
	return User.findById({_id:reqbody}).then((result,error)=>{
		let newData = {
			fullName: result.fullName,
			major: result.major,
			dateHired: result.dateHired
		}

		if(error) return false
		else return result 
	})
}

// Access Records by ID
module.exports.getRecordById = reqParams=>{
	return Record.findById({_id:reqParams.recordId}).then((result,error)=>{
		if(error) return false
		else return result 
	})
}

