const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	fullName: {
		type: String,
		trim: true,
		required: true
	},
	isAdmin: {
		type: Boolean,
		required: false,
		default: false
	},
	password: {
		type: String,
		required: true
	}
})

module.exports = mongoose.model("User", userSchema)