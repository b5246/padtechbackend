const mongoose = require('mongoose')

const recordSchema = new mongoose.Schema({
	fullName: {
		type: String,
		trim: true,
		required: true
	},
	major: {
		type: String,
		trim: true,
		required: true
	},
	dateHired:{
		type: String,
    	default: Date.now
	}
})

module.exports = mongoose.model("Record", recordSchema)